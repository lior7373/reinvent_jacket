#include "Arduino.h"

int delayval = 250; // delay for half a second
void initNeoPixel()
{
  rightNeoPixel.begin(); // This initializes the NeoPixel library.
  leftNeoPixel.begin();

  for(int i=0;i<NUMPIXELS;i++){
    rightNeoPixel.setPixelColor(i, getColor(&rightNeoPixel,'y'));
    rightNeoPixel.show(); 
	  leftNeoPixel.setPixelColor(i, getColor(&leftNeoPixel,'y'));
    leftNeoPixel.show(); 
    delay(delayval);
  }

   for(int i=0;i<NUMPIXELS;i++){
    rightNeoPixel.setPixelColor(i, getColor(&rightNeoPixel,'b')); 
    rightNeoPixel.show(); 
	  leftNeoPixel.setPixelColor(i, getColor(&leftNeoPixel,'b')); 
    leftNeoPixel.show(); 
    delay(delayval);
  }
	rightNeoPixel.setPixelColor(0, getColor(&rightNeoPixel,'p')); 
	rightNeoPixel.setPixelColor(7, getColor(&rightNeoPixel,'p')); 
	rightNeoPixel.show(); 
	leftNeoPixel.setPixelColor(0, getColor(&leftNeoPixel,'p')); 
	leftNeoPixel.setPixelColor(7, getColor(&leftNeoPixel,'p')); 
	leftNeoPixel.show(); 
}

void turnOnSideLight(char side)
{
	if(side == 'r'){
		setLight(&rightNeoPixel, getColor(&rightNeoPixel,'y'));
	}
	if(side == 'l'){
		setLight(&leftNeoPixel, getColor(&leftNeoPixel,'y'));
	}
   
}

void turnOffSideLight(char side)
{
	if(side == 'r'){
		setLight(&rightNeoPixel, getColor(&rightNeoPixel,'b'));
	}
	if(side == 'l'){
		setLight(&leftNeoPixel, getColor(&leftNeoPixel,'b'));
	}
}

void setLight(Adafruit_NeoPixel* strip , uint32_t color)
{
  for(int i=1;i<NUMPIXELS-1;i++){
    strip->setPixelColor(i, color); // Moderately bright green color.
    strip->show(); // This sends the updated pixel color to the hardware.
  }
}

uint32_t getColor(Adafruit_NeoPixel* strip ,byte color){
	switch(color){
		case 'r'  :
			return strip->Color(255,0,0);
		case 'p'  :
			return strip->Color(0,0,255);
        case 'b'  :
			return strip->Color(0,0,0);
		case 'g'  :
			return strip->Color(0,255,0);
        case 'y'  :
			return strip->Color(255,255,0);
		default : /* Optional */
			return strip->Color(0,0,0);
	}
}

