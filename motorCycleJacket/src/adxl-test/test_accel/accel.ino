#include <Wire.h>
#include <Arduino.h>

//----------accelerometer----------//
#define ADXL345 (0x53) // Device address as specified in data sheet //ADXL345 accelerometer
#define DATAX0 (0x32)       //X-Axis Data 0
//#define DATAX1 0x33       //X-Axis Data 1
//#define DATAY0 0x34       //Y-Axis Data 0
//#define DATAY1 0x35       //Y-Axis Data 1
//#define DATAZ0 0x36       //Z-Axis Data 0
//#define DATAZ1 0x37       //Z-Axis Data 1


//----------accelerometer----------//

//-------------compass-------------//
#define HMC5883 0x1E //0011110b, I2C 7bit address of HMC5883



//------------gyroscope------------//
#define ITG3200 0x68

//------------gyroscope------------//

int a_offx = 0;
int a_offy = 0;
int a_offz = 0;

int a_calibration_mode = 0;

void SetAccelerometer()
{
    // Put the ADXL345 into +/- 4G (now 16G) range by writing the value 0x01 (now 0B) to the DATA_FORMAT register.
    Wire.beginTransmission( ADXL345 );  // start transmission to device
    Wire.write( 0x31 );                 // send register address
    Wire.write( 0x0B );                 // send value to write
    Wire.endTransmission();             // end transmission
    
    //Put the ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTL register.
    Wire.beginTransmission( ADXL345 );     // start transmission to device
    Wire.write( 0x2D );                 // send register address  //Power Control Register
    Wire.write( 0x08 );                 // send value to write
    Wire.endTransmission();             // end transmission

    delay(10);
    AccelCalibrate();
}

void AccelCalibrate(){
    int count = 50;
    
    int tmpx = 0;
    int tmpy = 0;
    int tmpz = 0;
    
    a_offx = 0;
    a_offy = 0;
    a_offz = 0;

    a_calibration_mode = 1;
    
    for (uint8_t i = 0; i < count; i ++) //take the mean from 10 accelerometer probes and divide it from the current probe
    {
        delay(10);
        float* ap = readFromAccelerometer();
        tmpx += *(  ap);
        tmpy += *(++ap);
        tmpz += *(++ap);
    }
    a_offx = tmpx/count;
    a_offy = tmpy/count;
    a_offz = tmpz/count;

    a_calibration_mode = 0;

    Serial.println("accelerometer - calibration (offsets):");
    Serial.print(" x: ");    Serial.println(a_offx);
    Serial.print(" y: ");    Serial.println(a_offy);
    Serial.print(" z: ");    Serial.println(a_offz);    Serial.println();
}

float* readFromAccelerometer()
{
    static int axis[3];
    static float axisCorrected[3];
    int buff[6];
    
    Wire.beginTransmission( ADXL345 );      // start transmission to device
    Wire.write( DATAX0 );               	  // sends address to read from
    Wire.endTransmission();              	  // end transmission
    
    Wire.beginTransmission( ADXL345 );      // start transmission to device
    Wire.requestFrom( ADXL345, 6 );         // request 6 bytes from device
    
    uint8_t i = 0;
    while(Wire.available())                 // device may send less than requested (abnormal)
    {
        buff[i] = Wire.read();              // receive a byte
        i++;
    }
    Wire.endTransmission();                 // end transmission
    
    axis[0] = ((buff[1]) << 8) | buff[0];
    axis[1] = ((buff[3]) << 8) | buff[2];
    axis[2] = ((buff[5]) << 8) | buff[4];

    axisCorrected[0] = (axis[0] * 1.0) - a_offx;
    axisCorrected[1] = (axis[1] * 1.0) - a_offy;
    axisCorrected[2] = (axis[2] * 1.0) - a_offz;
    if (a_calibration_mode != 1)
      axisCorrected[2] += 256.0;
    
    return axisCorrected;
}

void SetCompass()
{
    //Put the HMC5883 IC into the correct operating mode
    Wire.beginTransmission( HMC5883 );//open communication with HMC5883
    Wire.write( 0x02 );               //select mode register
    Wire.write( 0x00 );               //continuous measurement mode
    Wire.endTransmission();
}

int* readFromCompass()
{
    static int axis[3];
    
    //Tell the HMC5883 where to begin reading data
    Wire.beginTransmission( HMC5883 );
    Wire.write( 0x03 );               //select register 3, X MSB register
    Wire.endTransmission();
    
    
    //Read data from each axis, 2 registers per axis
    Wire.requestFrom( HMC5883, 6 );
    if(6<=Wire.available()){
        axis[0] = Wire.read()<<8;   //X msb
        axis[0] |= Wire.read();     //X lsb
        axis[2] = Wire.read()<<8;   //Z msb
        axis[2] |= Wire.read();     //Z lsb
        axis[1] = Wire.read()<<8;   //Y msb
        axis[1] |= Wire.read();     //Y lsb
    }
    return axis;
}

int g_offx = 0;
int g_offy = 0;
int g_offz = 0;

void SetGyro() {
    Wire.beginTransmission( ITG3200 );
    Wire.write( 0x3E );
    Wire.write( 0x00 );
    Wire.endTransmission();
    
    Wire.beginTransmission( ITG3200 );
    Wire.write( 0x15 );
    Wire.write( 0x07 );
    Wire.endTransmission();
    
    Wire.beginTransmission( ITG3200 );
    Wire.write( 0x16 );
    Wire.write( 0x1E );   // +/- 2000 dgrs/sec, 1KHz, 1E, 19
    Wire.endTransmission();
    
    Wire.beginTransmission( ITG3200 );
    Wire.write( 0x17 );
    Wire.write( 0x00 );
    Wire.endTransmission();
    
    delay(10);
    
    GyroCalibrate();
}

void GyroCalibrate(){
    
    int tmpx = 0;
    int tmpy = 0;
    int tmpz = 0;
    
    g_offx = 0;
    g_offy = 0;
    g_offz = 0;
    
    for (uint8_t i = 0; i < 10; i ++) //take the mean from 10 gyro probes and divide it from the current probe
    {
        delay(10);
        float* gp = ReadGyro();
        tmpx += *(  gp);
        tmpy += *(++gp);
        tmpz += *(++gp);
    }
    g_offx = tmpx/10;
    g_offy = tmpy/10;
    g_offz = tmpz/10;
}

float* ReadGyro()
{
    static float axis[4];
    
    Wire.beginTransmission( ITG3200 );
    Wire.write( 0x1B );
    Wire.endTransmission();
    
    Wire.beginTransmission( ITG3200 );
    Wire.requestFrom( ITG3200, 8 );    // request 8 bytes from ITG3200
    
    int i = 0;
    uint8_t buff[8];
    while(Wire.available())
    {
        buff[i] = Wire.read();
        i++;
    }
    Wire.endTransmission();
    
    axis[0] = ((buff[4] << 8) | buff[5]) - g_offx;
    axis[1] = ((buff[2] << 8) | buff[3]) - g_offy;
    axis[2] = ((buff[6] << 8) | buff[7]) - g_offz;
    axis[3] = ((buff[0] << 8) | buff[1]); // temperature
    
    return axis;
}

