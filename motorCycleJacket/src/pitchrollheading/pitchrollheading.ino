#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Adafruit_BMP085_U.h>
#include <Adafruit_L3GD20_U.h>
#include <Adafruit_10DOF.h>
#include <Adafruit_NeoPixel.h>
/* Assign a unique ID to the sensors */
Adafruit_10DOF                dof   = Adafruit_10DOF();
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(30301);
Adafruit_LSM303_Mag_Unified   mag   = Adafruit_LSM303_Mag_Unified(30302);
Adafruit_BMP085_Unified       bmp   = Adafruit_BMP085_Unified(18001);

#define RIGHT_PIN	6
#define LEFT_PIN	8
#define RST_BUTTON 7

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      8
Adafruit_NeoPixel rightNeoPixel = Adafruit_NeoPixel(NUMPIXELS, RIGHT_PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel leftNeoPixel = Adafruit_NeoPixel(NUMPIXELS, LEFT_PIN, NEO_GRB + NEO_KHZ800);

// Update this with the correct SLP for accurate altitude measurements
float seaLevelPressure = SENSORS_PRESSURE_SEALEVELHPA;

// for button stuff
int buttonState;
int buttonStatecontinius = HIGH;

#define PITCH_LEFT     -5.0
#define PITCH_RIGHT     5.0

int currentRollState  = 0;
float oldRollReading  = 0.0;
float oldPitchReading = 0.0;
float roll;
float pitch;
float heading;

//    Initialises all the sensors used by this example
void initSensors()
{
  if(!accel.begin())
  {
    // There was a problem detecting the LSM303 ... check your connections 
    Serial.println(F("Ooops, no LSM303 detected ... Check your wiring!"));
    while(1);
  }
  if(!mag.begin())
  {
    // There was a problem detecting the LSM303 ... check your connections
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  }
  if(!bmp.begin())
  {
    // There was a problem detecting the BMP180 ... check your connections 
    Serial.println("Ooops, no BMP180 detected ... Check your wiring!");
    while(1);
  }
}

bool GetCurrentRollAndPitch(float *roll, float *pitch)
{ 
  sensors_event_t accel_event;
  sensors_vec_t orientation;
  
  accel.getEvent(&accel_event);
  
  if (dof.accelGetOrientation(&accel_event, &orientation))
  {
    *roll = orientation.roll;
    *pitch = orientation.pitch;
    
    return true;
  }

  return false;
}

bool GetCurrentHeading(float *heading)
{
  sensors_event_t mag_event;
  sensors_vec_t orientation;
  
  mag.getEvent(&mag_event);
  
  if (dof.magGetOrientation(SENSOR_AXIS_Z, &mag_event, &orientation))
  {
    *heading = orientation.heading;
    
    return true;
  }

  return false;
}

// loop through telemetry until values are within Epsilon,
//   then record them to global vars
bool ReachSteadyState(float *roll, float *pitch, float *heading)
{
  float roll_candidate = 0.0;
  float pitch_candidate = 0.0;
  float heading_candidate = 0.0;
  bool found_quiescence = false;
  int stay_counter = 0;
  int stay_counter_target = 5;
  float epsilon = 3.0;
  bool got_RP = false;
  bool got_P = false;
  bool got_H = false;

  currentRollState = 0;   // when calibrating set state to upright middle
  Serial.print(F("before GetCurrentRollAndPitch ")); Serial.println("");
  if (GetCurrentRollAndPitch(roll, pitch))
  {
    roll_candidate = *roll;
    pitch_candidate = *pitch;
    got_RP = true;
  }
  /*
	Serial.print(F("before GetCurrentHeading ")); Serial.println("");
  if (GetCurrentHeading(heading))
  {
    heading_candidate = *heading;
    got_H = true;
  }
  */

  while (!found_quiescence)
  {
	  Serial.println(F("inside while: !found_quiescence ")); 
	    Serial.print(F("    found_quiescence = ")); Serial.println(found_quiescence);
      Serial.print(F("    stay_counter = ")); Serial.println(stay_counter);
      Serial.print(F("    got_RP = ")); Serial.println(got_RP);
      Serial.println(F("    [roll, pitch] = ")); 
        Serial.print(F("        ")); Serial.println(*roll);
        Serial.print(F("        ")); Serial.println(*pitch);
      Serial.print(F("    roll_candidate = ")); Serial.println(roll_candidate);
    
	  delay(100);
    if (GetCurrentRollAndPitch(roll, pitch))
    {
      got_RP = true;
      if (abs(roll_candidate - *roll) > epsilon)  // not yet on roll
      {
        got_RP = false;
        stay_counter = 0;
      }
      else if (abs(pitch_candidate - *pitch) > epsilon)  // not yet on pitch
      {
        got_RP = false;
        stay_counter = 0;
      }
      // else if (GetCurrentHeading(heading))  // roll & pitch are good, let's try heading
      else if (1)
      {
        got_H = true;
        *heading = 0;   // only in the no heading mode , due to magnet in box
        if (abs(heading_candidate - *heading) > epsilon)  // not yet on heading
        {
          got_H = false;
          stay_counter = 0;
        }
        else // all 3 are possible
        {
          // update running average of values
          if (stay_counter < 1)
          {
            roll_candidate = *roll;
            pitch_candidate = *pitch;
            heading_candidate = *heading;
          }
          else
          {
            roll_candidate = (roll_candidate*stay_counter + *roll) / ((stay_counter+1) * 1.0);
            pitch_candidate = (pitch_candidate*stay_counter + *pitch) / ((stay_counter+1) * 1.0);
            heading_candidate = (heading_candidate*stay_counter + *heading) / ((stay_counter+1) * 1.0);
          }
          
          stay_counter++;
          found_quiescence = (stay_counter >= stay_counter_target);
          
          // reset for next
          got_RP = false;
          got_H = false;
        }
      }
    }
  }

  *roll = roll_candidate;
  *pitch = pitch_candidate;
  *heading = heading_candidate;

  return true;
}

/**************************************************************************/
/*!

*/
/**************************************************************************/
void setup(void)
{
  delay(5000);
  Serial.begin(9600);
  Serial.println(F("Adafruit 10 DOF Pitch/Roll/Heading Example")); Serial.println("");
  
  /* Initialise the sensors */
  initSensors();

  pinMode(RST_BUTTON , INPUT);
  // callibration
  // later we will set up callibration light pattern
  ReachSteadyState(&roll, &pitch, &heading);
  // and then success light pattern

  initNeoPixel();
  Serial.println(F("Done ")); Serial.println("");
}

/**************************************************************************/
/*!
    @brief  Constantly check the roll/pitch/heading/altitude/temperature
*/
/**************************************************************************/
void loop(void)
{
  /*
  sensors_event_t accel_event;
  sensors_event_t mag_event;
  sensors_event_t bmp_event;
  sensors_vec_t   orientation;
  */
  /* Calculate pitch and roll from the raw accelerometer data */
  /* accel.getEvent(&accel_event);
  if (dof.accelGetOrientation(&accel_event, &orientation))
  {
    // 'orientation' should have valid .roll and .pitch fields 
    Serial.print(F("Roll: "));
    Serial.print(orientation.roll);
    Serial.print(F("; "));
    Serial.print(F("Pitch: "));
    Serial.print(orientation.pitch);
    Serial.print(F("; "));
  }
  */
  /* Calculate the heading using the magnetometer */
  /*mag.getEvent(&mag_event);
  if (dof.magGetOrientation(SENSOR_AXIS_Z, &mag_event, &orientation))
  {
    // 'orientation' should have valid .heading data now 
    Serial.print(F("Heading: "));
    Serial.print(orientation.heading);
    Serial.print(F("; "));
  }
	*/
   int buttonState = digitalRead(RST_BUTTON);

  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (buttonState == HIGH ) {
    if(buttonStatecontinius == HIGH) {
      Serial.println(F("going to ReachSteadyState")); Serial.println("");
      ReachSteadyState(&roll, &pitch, &heading);
      Serial.println(F("finished ReachSteadyState")); Serial.println("");
      buttonStatecontinius = LOW;
    }
  } else {  // button isn't pressed right now
    buttonStatecontinius = HIGH;
  }
  
  // this is for in case we want to debug the calibration routine
  char inChar = Serial.read(); // Read a character
  if(inChar == 'r'){
    Serial.println(F("going to ReachSteadyState")); Serial.println("");
    ReachSteadyState(&roll, &pitch, &heading);
    Serial.println(F("finished ReachSteadyState")); Serial.println("");
  }

  // inside the rollToSide() function we do another read telemetry call
	int direction = rollToSide(); 
  if(direction == 1 ) {
	  turnOnSideLight('l');
	  turnOffSideLight('r');
  }
  else if ( direction == 2 ) {
		turnOnSideLight('r');
		turnOffSideLight('l');
  } else if ( direction == 0 ){
		turnOffSideLight('r');
		turnOffSideLight('l');
	}
 
  delay(100);
  
}

// detect roll state, by also doing another reading point
// can return 0 - middle : 1 - left : 2 - right : -1 - error
int rollToSide()
{
  float rollDeltaMax = 10.0;
  float pitchDeltaMax = 10.0;
  float roll_delta, pitch_delta;
    
  GetCurrentRollAndPitch(&roll, &pitch);
  // GetCurrentHeading(&heading);   // no magnet works
  roll_delta = roll - oldRollReading;
  pitch_delta = pitch - oldPitchReading;

  // adding a test for level state
  if ((pitch > PITCH_LEFT) && (pitch < PITCH_RIGHT))
    currentRollState = 0;
  
  Serial.println(F("in the main loop: "));
  Serial.print(F("    Roll: "));  Serial.println(roll);
  Serial.print(F("    Pitch: "));  Serial.println(pitch);
  Serial.print(F("    oldRollReading: "));  Serial.println(oldRollReading);
  Serial.print(F("    roll_delta: "));  Serial.println(roll_delta);
  Serial.print(F("    oldPitchReading: "));  Serial.println(oldPitchReading);
  Serial.print(F("    pitch_delta: "));  Serial.println(pitch_delta);
  Serial.print(F("    currentRoll state: "));  
  if (currentRollState == 0)
    Serial.println("middle");
  else if (currentRollState == 1)
    Serial.println("left");
  else if (currentRollState == 2)
    Serial.println("right");
  else
    Serial.println("unexpected");

  /*
  if (abs(roll_delta) > rollDeltaMax ) {  // a turning or return from turning detected
	  if(roll_delta > 0 ){
    
  if (abs(pitch_delta) > pitchDeltaMax ) {  // a turning or return from turning detected
    if(pitch_delta > 0 ){
      if (currentRollState == 0)
        currentRollState = 1;
          else if(currentRollState == 2)
            currentRollState = 0;
    } else {
      if (currentRollState == 0)
        currentRollState = 2;
      else if (currentRollState == 1)
        currentRollState = 0;
    }
  }
   */
  if (pitch < PITCH_LEFT)
    currentRollState = 2;
  else if (pitch > PITCH_RIGHT)
    currentRollState = 1;
    
  Serial.print(F("    updated currentRoll state: "));  
  if (currentRollState == 0)
    Serial.println("middle");
  else if (currentRollState == 1)
    Serial.println("left");
  else if (currentRollState == 2)
    Serial.println("right");
  else
    Serial.println("unexpected");

  oldRollReading = roll;
  oldPitchReading = pitch;
  return currentRollState;
}

