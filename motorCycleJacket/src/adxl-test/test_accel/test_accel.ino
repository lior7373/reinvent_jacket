#include <Wire.h>
#include "Arduino.h"

// #defun  SCALE_FACTOR  0.0078
// #define  SCALE_FACTOR  1.0
#define  SCALE_FACTOR  256.0

void setup()
{
    Wire.begin();
    Serial.begin(9600);
    SetAccelerometer();
    SetCompass();
    SetGyro();
}

void loop()
{
    float* accelp;
    int* compassp;
    float* gyrop;
    
    accelp = readFromAccelerometer();
    compassp = readFromCompass();
    gyrop = ReadGyro();

	// these numbers may only work for +/- 2G or 4G : they may not work for 16G
    float xg = *(  accelp) / SCALE_FACTOR;
    float yg = *(++accelp) / SCALE_FACTOR;
    float zg = *(++accelp) / SCALE_FACTOR;
    
    Serial.print("accelerometer - calibrated");
    Serial.print(" x:");
    Serial.print(xg);
    Serial.print(" y:");
    Serial.print(yg);
    Serial.print(" z:");
    Serial.print(zg);
    
//    Serial.print("  compass");
//    Serial.print(" x:");
//    Serial.print(*(  compassp));
//    Serial.print(" y:");
//    Serial.print(*(++compassp));
//    Serial.print(" z:");
//    Serial.print(*(++compassp));
    
    Serial.print("  gyro");
    Serial.print(" x:");
    Serial.print(*(  gyrop)/ 14.375);
    Serial.print(" y:");
    Serial.print(*(++gyrop)/ 14.375);
    Serial.print(" z:");
    Serial.print(*(++gyrop)/ 14.375);
    Serial.println();
//    Serial.print(" gyro temp:");
//    Serial.println(35+(*(++gyrop)+13200)/280);
//    
    delay(1000);             // only read every 0,5 seconds, 10ms for 100Hz, 20ms for 50Hz
}
